//
//  FFMpegNative.m
//  rnative_test1
//
//  Created by ingenuity on 09/08/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "FFMpegNative.h"
#import <mobileffmpeg/mobileffmpeg.h>

@implementation FFMpegNative

RCT_EXPORT_MODULE(RNFfmpegNative);

RCT_REMAP_METHOD(GetFFMpegVersion,
                 GetffmpegVersionWithResolver:(RCTPromiseResolveBlock)resolve
                 GetffmpegVersionrejecter:(RCTPromiseRejectBlock)reject)
{
  NSString* version = [NSString stringWithUTF8String:mobileffmpeg_get_ffmpeg_version()];
  if (version) {
    resolve(version);
  } else {
    NSError *error = [NSError errorWithDomain:@"" code:-1 userInfo:nil];
    reject(@"no_events", @"There were no events", error);
  }
}

RCT_REMAP_METHOD(GetLibraryVersion,
                 GetLibraryVersionWithResolver:(RCTPromiseResolveBlock)resolve
                 GetLibraryVersionrejecter:(RCTPromiseRejectBlock)reject)
{
  NSString* version = [NSString stringWithUTF8String:mobileffmpeg_get_version()];
  if (version) {
    resolve(version);
  } else {
    NSError *error = [NSError errorWithDomain:@"" code:-1 userInfo:nil];
    reject(@"no_events", @"There were no events", error);
  }
}

RCT_REMAP_METHOD(Execute,
                 args:(NSString *)args
                 ExecuteWithResolver:(RCTPromiseResolveBlock)resolve
                 Executerejecter:(RCTPromiseRejectBlock)reject)
{
  NSArray* commandArray = [args componentsSeparatedByString:@" "];
  char **arguments = (char **)malloc(sizeof(char*) * ([commandArray count]));
  for (int i=0; i < [commandArray count]; i++) {
    NSString *argument = [commandArray objectAtIndex:i];
    arguments[i] = (char *) [argument UTF8String];
  }
  
  NSNumber* result = [NSNumber numberWithInt:mobileffmpeg_execute((int) [commandArray count], arguments)];
  if (result) {
    resolve(result);
  } else {
    NSError *error = [NSError errorWithDomain:@"" code:-1 userInfo:nil];
    reject(@"no_events", @"There were no events", error);
  }
  
  free(arguments);
}

@end
