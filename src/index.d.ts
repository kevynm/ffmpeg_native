import { EmitterSubscription } from 'react-native';
export declare function GetffmpegVersion(): Promise<string>;
export declare function GetLibraryVersion(): Promise<string>;
/**
 * Cancels ffmpeg operation
 * @returns - returns true if successful, false if error
 */
export declare function Shutdown(): Promise<boolean>;
/**
 *
 * @param args - the ffmpeg string argument
 * @returns - true if successful, false if failed
 */
export declare function Execute(args: string): Promise<boolean>;
export declare function Statistics(callback: (data: StatisticsData) => void): EmitterSubscription;
export declare type StatisticsData = Readonly<{
    bitrate: number;
    size: number;
    speed: number;
    time: number;
    fps: number;
    frame: number;
    quality: number;
}>;
export declare function Info(callback: (info: {
    message: string;
}) => void): EmitterSubscription;
//# sourceMappingURL=index.d.ts.map