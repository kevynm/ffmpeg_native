import {
  NativeModules,
  DeviceEventEmitter,
  EmitterSubscription,
} from 'react-native';

const ffmpeg = NativeModules.RNFfmpegNative;

export function GetffmpegVersion(): Promise<string> {
  return ffmpeg.GetFFMpegVersion();
}

export function GetLibraryVersion(): Promise<string> {
  return ffmpeg.GetLibraryVersion();
}

/**
 * Cancels ffmpeg operation
 * @returns - returns true if successful, false if error
 */
export function Shutdown(): Promise<boolean> {
  // if (Platform.OS == 'android') {
  // }
  return ffmpeg.Shutdown();
  // const promise = new Promise<boolean>((resolve, reject) => resolve(false));
  // return promise;
}

/**
 *
 * @param args - the ffmpeg string argument
 * @returns - true if successful, false if failed
 */
export function Execute(args: string): Promise<boolean> {
  const promise: Promise<number> = ffmpeg.Execute(args);
  return promise.then(val => val === 0);
  // if (Platform.OS == 'ios') {
  // } else {
  //   return ffmpeg.Execute(args);
  // }
}

export function Statistics(
  callback: (data: StatisticsData) => void
): EmitterSubscription {
  return DeviceEventEmitter.addListener('statistics', callback);
}

export type StatisticsData = Readonly<{
  bitrate: number;
  size: number;
  speed: number;
  time: number;
  fps: number;
  frame: number;
  quality: number;
}>;

export function Info(
  callback: (info: { message: string }) => void
): EmitterSubscription {
  return DeviceEventEmitter.addListener('info', callback);
}

// this.addListenerOn(DeviceEventEmitter,
//   'keyboardWillShow',
//   this.scrollResponderKeyboardWillShow);
