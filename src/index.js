"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const ffmpeg = react_native_1.NativeModules.RNFfmpegNative;
function GetffmpegVersion() {
    return ffmpeg.GetFFMpegVersion();
}
exports.GetffmpegVersion = GetffmpegVersion;
function GetLibraryVersion() {
    return ffmpeg.GetLibraryVersion();
}
exports.GetLibraryVersion = GetLibraryVersion;
/**
 * Cancels ffmpeg operation
 * @returns - returns true if successful, false if error
 */
function Shutdown() {
    // if (Platform.OS == 'android') {
    // }
    return ffmpeg.Shutdown();
    // const promise = new Promise<boolean>((resolve, reject) => resolve(false));
    // return promise;
}
exports.Shutdown = Shutdown;
/**
 *
 * @param args - the ffmpeg string argument
 * @returns - true if successful, false if failed
 */
function Execute(args) {
    const promise = ffmpeg.Execute(args);
    return promise.then(val => val === 0);
    // if (Platform.OS == 'ios') {
    // } else {
    //   return ffmpeg.Execute(args);
    // }
}
exports.Execute = Execute;
function Statistics(callback) {
    return react_native_1.DeviceEventEmitter.addListener('statistics', callback);
}
exports.Statistics = Statistics;
function Info(callback) {
    return react_native_1.DeviceEventEmitter.addListener('info', callback);
}
exports.Info = Info;
// this.addListenerOn(DeviceEventEmitter,
//   'keyboardWillShow',
//   this.scrollResponderKeyboardWillShow);
//# sourceMappingURL=index.js.map