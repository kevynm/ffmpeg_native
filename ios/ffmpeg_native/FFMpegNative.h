//
//  FFMpegNative.h
//  rnative_test1
//
//  Created by ingenuity on 09/08/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface FFMpegNative : NSObject <RCTBridgeModule>
@end
