
package com.ingenuity;

import android.util.Log;

import com.arthenica.mobileffmpeg.Level;
import com.arthenica.mobileffmpeg.LogCallback;
import com.arthenica.mobileffmpeg.LogMessage;
import com.arthenica.mobileffmpeg.util.RunCallback;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;
import com.arthenica.mobileffmpeg.FFmpeg;
import com.arthenica.mobileffmpeg.Config;
import com.arthenica.mobileffmpeg.Statistics;
import com.arthenica.mobileffmpeg.StatisticsCallback;
import util.AsynchronousTaskService;

import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.uimanager.IllegalViewOperationException;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.annotation.Nullable;

public class RNFfmpegNativeModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  protected static final AsynchronousTaskService asynchronousTaskService = new AsynchronousTaskService();

  /**
   * <p>Starts a new asynchronous FFmpeg operation with arguments provided.
   *
   * @param runCallback callback function to receive result of this execution
   * @param arguments FFmpeg command options/arguments
   * @return <code>Future</code> instance of asynchronous operation started
   */
  public static Future executeAsync(final RunCallback runCallback, final String arguments) {
    return asynchronousTaskService.runAsynchronously(new Callable<Integer>() {

      @Override
      public Integer call() {
        Log.d(Config.TAG, "async execute " + arguments);
        int returnCode = FFmpeg.execute(arguments);
        if (runCallback != null) {
          runCallback.apply(returnCode);
        }

        return returnCode;
      }
    });
  }

  public RNFfmpegNativeModule(final ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;

    Config.setLogLevel(Level.AV_LOG_VERBOSE);

    //initialize ffmpeg statistics callback
    Config.enableStatisticsCallback(new StatisticsCallback() {
      public void apply(Statistics newStatistics) {

        double bitrate = newStatistics.getBitrate();
        long size = newStatistics.getSize();
        double speed = newStatistics.getSpeed();
        int time = newStatistics.getTime();
        float fps = newStatistics.getVideoFps();
        int frame = newStatistics.getVideoFrameNumber();
        float quality = newStatistics.getVideoQuality();

        WritableMap params = Arguments.createMap();
        params.putDouble("bitrate", bitrate);
        params.putDouble("size", new Long(size).doubleValue());
        params.putDouble("speed", speed);
        params.putInt("time", time);
        params.putDouble("fps", fps);
        params.putInt("frame", frame);
        params.putDouble("quality", quality);

        sendEvent(reactContext,"statistics", params);
      }
    });

    //enable log callback
    Config.enableLogCallback(new LogCallback() {
      public void apply(LogMessage message) {
        Log.d(Config.TAG, "message: " + message.getText());
        WritableMap params = Arguments.createMap();
        params.putString("message", message.getText());
        sendEvent(reactContext, "info", params);
      }
    });
  }

  private void sendEvent(ReactContext reactContext,
                         String eventName,
                         @Nullable WritableMap params) {
    reactContext
            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
            .emit(eventName, params);
  }

  @Override
  public String getName() {
    return "RNFfmpegNative";
  }

  @ReactMethod
  public void GetFFMpegVersion(Promise promise) {
      String version = FFmpeg.getFFmpegVersion();
      try{
        promise.resolve(version);
      }catch(IllegalViewOperationException e){
        promise.reject(e.getMessage(), e);
      }
  }

  @ReactMethod
  public void GetLibraryVersion(Promise promise){
    String version = FFmpeg.getVersion();
    try {
      promise.resolve(version);
    }catch(IllegalViewOperationException e){
      promise.reject(e.getMessage(), e);
    }
  }

  @ReactMethod
  public void Shutdown(Promise promise){
    try {
      FFmpeg.cancel();
      promise.resolve(true);
    }catch(IllegalViewOperationException e){
      promise.reject(e.getMessage(), e);
    }
  }

  @ReactMethod
  public void Execute(String arguments, final Promise promise) {
    try {
      Log.d(Config.TAG, "executing: " + arguments);
      Config.resetStatistics();

      executeAsync(new RunCallback() {
        @Override
        public void apply(final int returnCode) {
          if (returnCode == FFmpeg.RETURN_CODE_SUCCESS){
            Log.d(Config.TAG, "Command execution completed successfully.");
          }
          else if (returnCode == FFmpeg.RETURN_CODE_CANCEL){
            Log.d(Config.TAG, "Command execution cancelled by user");
          }
          promise.resolve(returnCode);
        }
      }, arguments);
    }catch(IllegalViewOperationException e){
      Log.e(Config.TAG, "error " + e.getMessage());
      promise.reject(e.getMessage(), e);
    }
  }
}